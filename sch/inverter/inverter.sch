v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 300 150 300 210 { lab=out}
N 220 240 260 240 { lab=in}
N 220 120 220 240 { lab=in}
N 220 120 260 120 { lab=in}
N 180 180 220 180 { lab=in}
N 300 180 400 180 { lab=out}
N 300 120 380 120 { lab=vdd}
N 380 60 380 120 { lab=vdd}
N 300 60 380 60 { lab=vdd}
N 300 60 300 90 { lab=vdd}
N 300 30 300 60 { lab=vdd}
N 300 240 370 240 { lab=vss}
N 370 240 380 240 { lab=vss}
N 380 240 380 300 { lab=vss}
N 300 300 380 300 { lab=vss}
N 300 270 300 300 { lab=vss}
N 300 300 300 340 { lab=vss}
C {sky130_fd_pr/nfet_01v8.sym} 280 240 0 0 {name=M1
L=0.15
W=0.45
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 280 120 0 0 {name=M2
L=0.15
W=\{wp\}
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {ipin.sym} 180 180 0 0 {name=p1 lab=in}
C {opin.sym} 400 180 0 0 {name=p2 lab=out}
C {ipin.sym} 300 30 1 0 {name=p3 lab=vdd
}
C {ipin.sym} 300 340 3 0 {name=p4 lab=vss}
C {param.sym} 440 140 0 0 {name=s1 value="wp=1.5"}
