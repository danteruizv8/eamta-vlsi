v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -570 220 -570 260 { lab=GND}
N -570 110 -570 160 { lab=vss}
N -480 110 -480 160 { lab=vdd}
N -380 110 -380 160 { lab=vin}
N -380 220 -380 270 { lab=vss}
N -480 220 -480 270 { lab=vss}
N 280 -0 280 90 { lab=out}
N 280 150 280 190 { lab=vss}
N 0 -140 0 -70 { lab=vdd}
N 0 70 0 130 { lab=vss}
N -140 0 -80 -0 { lab=vin}
N 220 0 280 0 { lab=out}
N 180 0 220 0 { lab=out}
C {/home/dante/caravel_fulgor_opamp/xschem/inverter/inverter.sym} 0 0 0 0 {name=x1}
C {vsource.sym} -570 190 0 0 {name=V1 value=0
}
C {vsource.sym} -480 190 0 0 {name=V2 value=1.8
}
C {vsource.sym} -380 190 0 0 {name=V3 value="PULSE(0 \{Vin\} 1ps 1ps 1ps 50ns 100ns) DC\{Vin\}"}
C {gnd.sym} -570 260 0 0 {name=l1 lab=GND}
C {capa.sym} 280 120 0 0 {name=C1
m=1
value=1f
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} -570 110 0 0 {name=l3 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -480 110 0 0 {name=l4 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} -380 110 0 0 {name=l5 sig_type=std_logic lab=vin
}
C {lab_pin.sym} -380 270 0 0 {name=l6 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -480 270 0 0 {name=l7 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -140 0 0 0 {name=l8 sig_type=std_logic lab=vin
}
C {lab_pin.sym} 0 -140 0 0 {name=l9 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 0 130 0 0 {name=l10 sig_type=std_logic lab=vss
}
C {lab_pin.sym} 280 190 0 0 {name=l2 sig_type=std_logic lab=vss
}
C {netlist_not_shown.sym} -380 -120 0 0 {name=simulacion only_toplevel=false value="
* Dante Ruiz - EAMTA 2021
* Parametros del circuito

.param Vin=1.8
.param wp=1
.options TEMP = 27.0

* include
.lib ~/skywater/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest/models/corners/sky130.lib TT

*Signals to save

.save all
+ @M.X1.XM1.msky130_fd_pr__nfet_01v8[id] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gm]
+ @M.X1.XM2.msky130_fd_pr__pfet_01v8[id] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gm]
* simulacion
.control
  tran 0.1n 0.5us
  setplot tran1
  plot v(out) v(vin)
  write tb_inverter_tran.raw



  
  reset
  dc V3 0 2.0 0.01
  setplot dc1
  plot v(out) v(vin)
  write tb_inverter_dc.raw

  reset
  op
  write tb_inverter.raw
  print all

  reset
  dc lin V3 0 2.0 0.01
  step param wp lin 5 0.45 1.5
  plot v(out)

.endc

.end

"}
C {lab_pin.sym} 220 0 1 0 {name=l11 sig_type=std_logic lab=out
}
