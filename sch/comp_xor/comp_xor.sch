v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -170 210 -160 210 { lab=#net1}
N -160 180 -160 210 { lab=#net1}
N -170 180 -160 180 { lab=#net1}
N -170 300 -160 300 { lab=vss}
N -160 270 -160 300 { lab=vss}
N -170 270 -160 270 { lab=vss}
N 20 210 30 210 { lab=#net2}
N 20 180 20 210 { lab=#net2}
N 20 180 30 180 { lab=#net2}
N 20 300 30 300 { lab=vss}
N 20 270 20 300 { lab=vss}
N 20 270 30 270 { lab=vss}
N 330 70 340 70 { lab=vss}
N 340 40 340 70 { lab=vss}
N 330 40 340 40 { lab=vss}
N 520 70 530 70 { lab=vss}
N 520 40 520 70 { lab=vss}
N 520 40 530 40 { lab=vss}
N -170 -50 -160 -50 { lab=vdd}
N -160 -80 -160 -50 { lab=vdd}
N -170 -80 -160 -80 { lab=vdd}
N -170 40 -160 40 { lab=#net3}
N -160 10 -160 40 { lab=#net3}
N -170 10 -160 10 { lab=#net3}
N 20 -50 30 -50 { lab=vdd}
N 20 -80 20 -50 { lab=vdd}
N 20 -80 30 -80 { lab=vdd}
N 20 40 30 40 { lab=#net3}
N 20 10 20 40 { lab=#net3}
N 20 10 30 10 { lab=#net3}
N 330 -50 340 -50 { lab=vdd}
N 340 -80 340 -50 { lab=vdd}
N 330 -80 340 -80 { lab=vdd}
N 520 -50 530 -50 { lab=vdd}
N 520 -80 520 -50 { lab=vdd}
N 520 -80 530 -80 { lab=vdd}
N -170 -140 -170 -80 { lab=vdd}
N -170 -140 530 -140 { lab=vdd}
N 530 -140 530 -80 { lab=vdd}
N 330 -140 330 -80 { lab=vdd}
N 30 -140 30 -80 { lab=vdd}
N -170 -20 -170 10 { lab=#net3}
N 30 -20 30 10 { lab=#net3}
N -170 -10 30 -10 { lab=#net3}
N -170 70 -170 150 { lab=z}
N 30 70 30 150 { lab=z}
N -170 100 30 100 { lab=z}
N -170 210 -170 240 { lab=#net1}
N 30 210 30 240 { lab=#net2}
N -170 300 -170 330 { lab=vss}
N -170 330 30 330 { lab=vss}
N 30 300 30 330 { lab=vss}
N 330 -20 330 10 { lab=not_b}
N 530 -20 530 10 { lab=not_a}
N 330 70 330 100 { lab=vss}
N 330 100 530 100 { lab=vss}
N 530 70 530 100 { lab=vss}
N 180 -170 180 -140 { lab=vdd}
N 30 330 530 330 { lab=vss}
N 530 100 530 330 { lab=vss}
N 180 330 180 360 { lab=vss}
N -240 40 -210 40 { lab=in_a}
N 570 -50 600 -50 { lab=in_a}
N 600 -50 600 40 { lab=in_a}
N 570 40 600 40 { lab=in_a}
N -240 -50 -210 -50 { lab=not_b}
N 70 -50 100 -50 { lab=not_a}
N 30 100 100 100 { lab=z}
N 70 270 100 270 { lab=in_b}
N -240 180 -210 180 { lab=not_a}
N -240 270 -210 270 { lab=not_b}
N 600 -10 630 -10 { lab=in_a}
N 120 150 120 180 { lab=in_a}
N 630 -10 630 150 { lab=in_a}
N 490 -0 530 -0 { lab=not_a}
N 330 0 360 -0 { lab=not_b}
N 120 150 630 150 { lab=in_a}
N 100 100 130 100 { lab=z}
N 250 -50 290 -50 { lab=in_b}
N 250 -50 250 40 { lab=in_b}
N 250 40 290 40 { lab=in_b}
N 90 -10 90 40 { lab=in_b}
N 90 -10 250 -10 { lab=in_b}
N 70 180 140 180 { lab=in_a}
N 70 40 140 40 { lab=in_b}
C {sky130_fd_pr/nfet_01v8.sym} -190 180 0 0 {name=M4
L=L1
W=W2

nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -190 270 0 0 {name=M6
L=L1
W=W2

nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 50 180 0 1 {name=M5
L=L1
W=W2

nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 50 270 0 1 {name=M7
L=L1
W=W2
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 310 40 0 0 {name=M9
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 550 40 0 1 {name=M11
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -190 -50 0 0 {name=M0
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -190 40 0 0 {name=M2
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 50 -50 0 1 {name=M1
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 50 40 0 1 {name=M3
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 310 -50 0 0 {name=M8
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 550 -50 0 1 {name=M10
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {ipin.sym} 130 180 2 0 {name=p1 lab=in_a}
C {ipin.sym} 180 -170 1 0 {name=p2 lab=vdd
}
C {ipin.sym} 130 40 2 0 {name=p3 lab=in_b}
C {ipin.sym} 180 360 3 0 {name=p5 lab=vss
}
C {opin.sym} 130 100 0 0 {name=p4 lab=z
}
C {lab_pin.sym} 490 0 0 0 {name=l1 sig_type=std_logic lab=not_a
}
C {lab_pin.sym} 360 0 2 0 {name=l2 sig_type=std_logic lab=not_b
}
C {lab_pin.sym} 100 270 2 0 {name=l5 sig_type=std_logic lab=in_b}
C {lab_pin.sym} -240 270 0 0 {name=l6 sig_type=std_logic lab=not_b
}
C {lab_pin.sym} -240 180 0 0 {name=l7 sig_type=std_logic lab=not_a
}
C {lab_pin.sym} -240 40 0 0 {name=l8 sig_type=std_logic lab=in_a
}
C {lab_pin.sym} -240 -50 0 0 {name=l9 sig_type=std_logic lab=not_b
}
C {lab_pin.sym} 100 -50 2 0 {name=l10 sig_type=std_logic lab=not_a
}
