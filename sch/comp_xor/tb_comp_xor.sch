v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -550 320 -550 360 { lab=GND}
N -550 210 -550 260 { lab=vss}
N -550 20 -550 70 { lab=vdd}
N -440 210 -440 260 { lab=vin_b}
N -440 320 -440 370 { lab=vss}
N -550 130 -550 180 { lab=vss}
N 140 30 140 120 { lab=out}
N 140 180 140 220 { lab=vss}
N -40 -110 -40 -40 { lab=vdd}
N -40 100 -40 160 { lab=vss}
N -160 -10 -100 -10 { lab=vin_a}
N -160 60 -100 60 { lab=vin_b}
N -440 20 -440 70 { lab=vin_a}
N -440 130 -440 180 { lab=vss}
N 40 30 120 30 { lab=out}
N 120 30 140 30 { lab=out}
C {vsource.sym} -550 290 0 0 {name=V1 value=0
}
C {vsource.sym} -550 100 0 0 {name=V2 value=1.8
}
C {vsource.sym} -440 290 0 0 {name=V3 value="PULSE(0 \{Vin\} 1ps 1ps 1ps 100ns 200ns) DC\{Vin\}"}
C {gnd.sym} -550 360 0 0 {name=l1 lab=GND}
C {capa.sym} 140 150 0 0 {name=C1
m=1
value=1f
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} -550 210 0 0 {name=l3 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -550 20 0 0 {name=l4 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} -440 210 0 0 {name=l5 sig_type=std_logic lab=vin_b
}
C {lab_pin.sym} -440 370 0 0 {name=l6 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -550 180 0 0 {name=l7 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -160 -10 0 0 {name=l8 sig_type=std_logic lab=vin_a
}
C {lab_pin.sym} -40 -110 0 0 {name=l9 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} -40 160 0 0 {name=l10 sig_type=std_logic lab=vss
}
C {lab_pin.sym} 140 220 0 0 {name=l2 sig_type=std_logic lab=vss
}
C {netlist_not_shown.sym} -390 -80 0 0 {name=simulacion only_toplevel=false value="
* Dante Ruiz - EAMTA 2021
* Parametros del circuito

.param Vin=1.8
.options TEMP = 27.0

* include
.lib ~/skywater/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest/models/corners/sky130.lib TT

.model adc_buff adc_bridge(in_low = 0.3 in_high = 1.5)

* Signals to save
.save all
* .save vin_a vin_b out
* + @M.X1.XM1.msky130_fd_pr__nfet_01v8[id] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gm]
* + @M.X1.XM2.msky130_fd_pr__pfet_01v8[id] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gm]
* simulacion
.control
  tran 0.1n 0.5us
  setplot tran1
  plot v(out) v(vin_a)+2 v(vin_b)+4
  write tb_comp_xor.raw
* gnuplot salida out vin_a vin_b
* eprvcd d_vin_a d_vin_b d_out > tb_comp_xor.vcd
.endc

.end

"}
C {lab_pin.sym} 120 30 1 0 {name=l11 sig_type=std_logic lab=out
}
C {lab_pin.sym} -160 60 0 0 {name=l12 sig_type=std_logic lab=vin_b
}
C {vsource.sym} -440 100 0 0 {name=V4 value="PULSE(0 \{Vin\} 1ps 1ps 1ps 50ns 100ns) DC\{Vin\}"}
C {lab_pin.sym} -440 20 0 0 {name=l13 sig_type=std_logic lab=vin_a}
C {lab_pin.sym} -440 180 0 0 {name=l14 sig_type=std_logic lab=vss
}
C {/home/dante/caravel_fulgor_opamp/xschem/comp_xor/comp_xor.sym} -70 30 0 0 {name=x1 L1=0.15 W1=0.45 W2=0.9}
