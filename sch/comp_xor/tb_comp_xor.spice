**.subckt tb_comp_xor
V1 vss GND 0
V2 vdd vss 1.8
V3 vin_b vss PULSE(0 {Vin} 1ps 1ps 1ps 100ns 200ns) DC{Vin} 
C1 out vss 1f m=1
V4 vin_a vss PULSE(0 {Vin} 1ps 1ps 1ps 50ns 100ns) DC{Vin} 
A2 [ out ] [ d_out ] adc_buff
A3 [ vin_b ] [ d_vin_b ] adc_buff
A4 [ vin_a ] [ d_vin_a ] adc_buff
x1 vdd vin_b out vin_a vss comp_xor
**** begin user architecture code


* Dante Ruiz - EAMTA 2021
* Parametros del circuito

.param Vin=1.8
.options TEMP = 27.0

* include
.lib ~/skywater/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest/models/corners/sky130.lib TT

.model adc_buff adc_bridge(in_low = 0.3 in_high = 1.5)

* Signals to save
.save all
* .save vin_a vin_b out
* + @M.X1.XM1.msky130_fd_pr__nfet_01v8[id] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gm]
* + @M.X1.XM2.msky130_fd_pr__pfet_01v8[id] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gm]
* simulacion
.control
  tran 0.1n 0.5us
  setplot tran1
  plot v(out) v(vin_a)+2 v(vin_b)+4
  write tb_comp_xor.raw
* gnuplot salida out vin_a vin_b
* eprvcd d_vin_a d_vin_b d_out > tb_comp_xor.vcd
.endc




**** end user architecture code
**.ends

* expanding   symbol:  /home/dante/caravel_fulgor_opamp/xschem/comp_xor/comp_xor.sym # of pins=5
* sym_path: /home/dante/caravel_fulgor_opamp/xschem/comp_xor/comp_xor.sym
* sch_path: /home/dante/caravel_fulgor_opamp/xschem/comp_xor/comp_xor.sch
.subckt comp_xor  vdd in_b z in_a vss
*.ipin in_a
*.ipin vdd
*.ipin in_b
*.ipin vss
*.opin z
XM1 z not_a net1 net1 sky130_fd_pr__nfet_01v8 L=0.15 W=0.45 nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM2 net1 not_b vss vss sky130_fd_pr__nfet_01v8 L=0.15 W=0.45 nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM3 z in_a net2 net2 sky130_fd_pr__nfet_01v8 L=0.15 W=0.45 nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM4 net2 in_b vss vss sky130_fd_pr__nfet_01v8 L=0.15 W=0.45 nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM5 not_b in_b vss vss sky130_fd_pr__nfet_01v8 L=0.15 W=0.45 nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM6 not_a in_a vss vss sky130_fd_pr__nfet_01v8 L=0.15 W=0.45 nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM7 net3 not_b vdd vdd sky130_fd_pr__pfet_01v8 L=0.15 W='wp' nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM8 z in_a net3 net3 sky130_fd_pr__pfet_01v8 L=0.15 W='wp' nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM9 net3 not_a vdd vdd sky130_fd_pr__pfet_01v8 L=0.15 W='wp' nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM10 z in_b net3 net3 sky130_fd_pr__pfet_01v8 L=0.15 W='wp' nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM11 not_b in_b vdd vdd sky130_fd_pr__pfet_01v8 L=0.15 W='wp' nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
XM12 not_a in_a vdd vdd sky130_fd_pr__pfet_01v8 L=0.15 W='wp' nf=1 ad='int((nf+1)/2) * W/nf * 0.29' as='int((nf+2)/2) * W/nf * 0.29'
+ pd='2*int((nf+1)/2) * (W/nf + 0.29)' ps='2*int((nf+2)/2) * (W/nf + 0.29)' nrd='0.29 / W' nrs='0.29 / W'
+ sa=0 sb=0 sd=0 mult=1 m=1 
.param wp=0.9
.ends

.GLOBAL GND
** flattened .save nodes
.end
