#
# Plotea tres seniales estilo test bench
#
set multiplot layout 3, 1 title "Test Bench 3 signals" font ",14"
set tmargin 1
set lmargin 10
set title
set xlabel
#unset xtics
unset key
set style data lines

# 
set ylabel "DATA 1"
plot 'salida.data' using 1:2
#
set tmargin 0
set ylabel "DATA 2"
plot 'salida.data' using 3:4 
#
set xtics
set ylabel "DATA 3"
plot 'salida.data' using 5:6 
#
unset multiplot
#
#

