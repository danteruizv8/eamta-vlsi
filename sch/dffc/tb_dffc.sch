v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -570 360 -570 400 { lab=GND}
N -570 250 -570 300 { lab=vss}
N -570 60 -570 110 { lab=vdd}
N -460 250 -460 300 { lab=vin}
N -460 360 -460 410 { lab=vss}
N -570 170 -570 220 { lab=vss}
N 130 20 130 110 { lab=out}
N 130 170 130 210 { lab=vss}
N -220 30 -160 30 { lab=vin}
N -220 80 -160 80 { lab=v_clk}
N -460 60 -460 110 { lab=v_clk}
N -460 170 -460 220 { lab=vss}
N 30 20 110 20 { lab=out}
N 110 20 130 20 { lab=out}
N 70 170 70 210 { lab=vss}
N 50 90 70 90 { lab=outb}
N 30 90 50 90 { lab=outb}
N 70 90 70 110 { lab=outb}
N -40 -40 -40 -20 { lab=clr}
N -80 -70 -80 -20 { lab=vdd}
N -80 130 -80 180 { lab=vss}
N -80 230 -80 280 { lab=clr}
N -80 340 -80 390 { lab=vss}
N -220 110 -160 110 { lab=not_clk}
C {vsource.sym} -570 330 0 0 {name=V1 value=0
}
C {vsource.sym} -570 140 0 0 {name=V2 value=1.8
}
C {vsource.sym} -460 330 0 0 {name=V3 value="PULSE(0 \{Vin\} 1ps 1ps 1ps 30ns 45ns) DC\{Vin\}"}
C {gnd.sym} -570 400 0 0 {name=l1 lab=GND}
C {capa.sym} 130 140 0 0 {name=C1
m=1
value=1f
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} -570 250 0 0 {name=l3 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -570 60 0 0 {name=l4 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} -460 250 0 0 {name=l5 sig_type=std_logic lab=vin
}
C {lab_pin.sym} -460 410 0 0 {name=l6 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -570 220 0 0 {name=l7 sig_type=std_logic lab=vss
}
C {lab_pin.sym} -220 30 0 0 {name=l8 sig_type=std_logic lab=vin
}
C {lab_pin.sym} 130 210 0 0 {name=l2 sig_type=std_logic lab=vss
}
C {netlist_not_shown.sym} -390 -80 0 0 {name=simulacion only_toplevel=false value="
* Dante Ruiz - EAMTA 2021
* Parametros del circuito

.param Vin=1.8
.options TEMP = 57.0

* include
.lib ~/skywater/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest/models/corners/sky130.lib TT

* .model adc_buff adc_bridge(in_low = 0.3 in_high = 1.5)

* Signals to save
.save out vin v_clk outb clr
* + @M.X1.XM1.msky130_fd_pr__nfet_01v8[id] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gm]
* + @M.X1.XM2.msky130_fd_pr__pfet_01v8[id] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gm]
* simulacion
.control
  tran 0.1n 1.5us
  setplot tran1
  plot v(out) v(vin)+2 v(v_clk)+4 v(clr)+6
  write tb_dffc.raw
* gnuplot salida out vin_a vin_b

.endc

.end

"}
C {lab_pin.sym} 110 20 1 0 {name=l11 sig_type=std_logic lab=out
}
C {lab_pin.sym} -220 80 0 0 {name=l12 sig_type=std_logic lab=v_clk
}
C {lab_pin.sym} -460 60 0 0 {name=l13 sig_type=std_logic lab=v_clk}
C {lab_pin.sym} -460 220 0 0 {name=l14 sig_type=std_logic lab=vss
}
C {/home/dante/caravel_fulgor_opamp/xschem/dffc/dffc.sym} -40 210 0 0 {name=x1 L1=0.15 W1=0.45 W2=0.9 W3=1.8}
C {capa.sym} 70 140 0 0 {name=C2
m=1
value=1f
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} 70 210 0 0 {name=l15 sig_type=std_logic lab=vss
}
C {lab_pin.sym} 50 90 1 0 {name=l16 sig_type=std_logic lab=outb
}
C {lab_pin.sym} -40 -40 1 0 {name=l17 sig_type=std_logic lab=clr}
C {lab_pin.sym} -80 -70 0 0 {name=l9 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} -80 180 0 0 {name=l10 sig_type=std_logic lab=vss
}
C {vsource.sym} -460 140 0 0 {name=V5 value="PULSE(0 \{Vin\} 10ns 1ps 1ps 50ns 100ns) DC\{Vin\}"}
C {lab_pin.sym} -80 230 0 0 {name=l18 sig_type=std_logic lab=clr}
C {lab_pin.sym} -80 390 0 0 {name=l19 sig_type=std_logic lab=vss
}
C {vsource.sym} -80 310 0 0 {name=V7 value="PULSE(0 \{Vin\} 500ns 1ps 1ps 500ns 1000ns) DC\{Vin\}"}
C {/home/dante/caravel_fulgor_opamp/xschem/inverter/inverter.sym} 360 70 0 0 {name=x2}
C {lab_pin.sym} 280 70 0 0 {name=l20 sig_type=std_logic lab=v_clk
}
C {lab_pin.sym} 360 0 0 0 {name=l21 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 360 140 0 0 {name=l22 sig_type=std_logic lab=vss
}
C {lab_pin.sym} 540 70 2 0 {name=l23 sig_type=std_logic lab=not_clk
}
C {lab_pin.sym} -220 110 0 0 {name=l24 sig_type=std_logic lab=not_clk
}
