v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 260 380 260 400 { lab=clk}
N 260 120 260 150 { lab=n_clk}
N 1480 130 1480 150 { lab=clk}
N 1480 380 1480 410 { lab=n_clk}
N -50 -380 -50 -360 { lab=clk}
N -50 -130 -50 -100 { lab=clk}
N 250 -250 970 -250 { lab=NET_M2_M11}
N 190 -200 210 -200 { lab=NET_M0_M5}
N 190 -310 190 -200 { lab=NET_M0_M5}
N 190 -310 210 -310 { lab=NET_M0_M5}
N 250 -280 250 -230 { lab=NET_M2_M11}
N 250 -200 260 -200 { lab=vss}
N 260 -200 260 -170 { lab=vss}
N 250 -170 260 -170 { lab=vss}
N 250 -310 260 -310 { lab=vdd}
N 260 -340 260 -310 { lab=vdd}
N 250 -340 260 -340 { lab=vdd}
N 250 -400 250 -340 { lab=vdd}
N 250 -170 250 -120 { lab=vss}
N 10 -250 190 -250 { lab=NET_M0_M5}
N 1790 30 1810 30 { lab=q}
N 1790 -80 1790 30 { lab=q}
N 1790 -80 1810 -80 { lab=q}
N 1850 -50 1850 0 { lab=qb}
N 1850 30 1860 30 { lab=vss}
N 1860 30 1860 60 { lab=vss}
N 1850 60 1860 60 { lab=vss}
N 1850 -80 1860 -80 { lab=vdd}
N 1860 -110 1860 -80 { lab=vdd}
N 1850 -110 1860 -110 { lab=vdd}
N 1850 -170 1850 -110 { lab=vdd}
N 1850 60 1850 110 { lab=vss}
N 60 -250 60 220 { lab=NET_M0_M5}
N 60 220 220 220 { lab=NET_M0_M5}
N -250 -250 -90 -250 { lab=d}
N 1330 -370 1330 -330 { lab=NET_M12_M13}
N 1230 -250 1230 -200 { lab=q}
N 1230 -250 1440 -250 { lab=q}
N 1440 -250 1440 -200 { lab=q}
N 1330 -270 1330 -250 { lab=q}
N 1170 -170 1190 -170 { lab=NET_M10_M19}
N 1170 -300 1170 -170 { lab=NET_M10_M19}
N 1170 -300 1290 -300 { lab=NET_M10_M19}
N 1370 -400 1500 -400 { lab=clr}
N 1500 -400 1500 -170 { lab=clr}
N 1480 -170 1500 -170 { lab=clr}
N 1230 -140 1230 -110 { lab=vss}
N 1230 -110 1440 -110 { lab=vss}
N 1440 -140 1440 -110 { lab=vss}
N 1330 -110 1330 -80 { lab=vss}
N 320 220 530 220 { lab=NET_M4_M9}
N 630 100 630 140 { lab=NET_M6_M7}
N 530 220 530 270 { lab=NET_M4_M9}
N 530 220 740 220 { lab=NET_M4_M9}
N 740 220 740 270 { lab=NET_M4_M9}
N 630 200 630 220 { lab=NET_M4_M9}
N 470 300 490 300 { lab=clr}
N 470 170 470 300 { lab=clr}
N 470 170 590 170 { lab=clr}
N 670 70 800 70 { lab=NET_M2_M11}
N 800 70 800 300 { lab=NET_M2_M11}
N 780 300 800 300 { lab=NET_M2_M11}
N 530 330 530 360 { lab=vss}
N 530 360 740 360 { lab=vss}
N 740 330 740 360 { lab=vss}
N 630 360 630 390 { lab=vss}
N 800 -250 800 70 { lab=NET_M2_M11}
N 1070 -250 1170 -250 { lab=NET_M10_M19}
N 1100 220 1440 220 { lab=NET_M10_M19}
N 1430 -250 2070 -250 { lab=q}
N 1540 220 2070 220 { lab=qb}
N 1330 -470 1330 -430 { lab=vdd}
N 630 -0 630 40 { lab=vdd}
N 530 300 540 300 { lab=vss}
N 540 300 540 330 { lab=vss}
N 530 330 540 330 { lab=vss}
N 730 300 740 300 { lab=vss}
N 730 300 730 330 { lab=vss}
N 730 330 740 330 { lab=vss}
N 630 170 640 170 { lab=NET_M6_M7}
N 640 140 640 170 { lab=NET_M6_M7}
N 630 140 640 140 { lab=NET_M6_M7}
N 620 70 630 70 { lab=vdd}
N 620 40 620 70 { lab=vdd}
N 620 40 630 40 { lab=vdd}
N 1320 -400 1330 -400 { lab=vdd}
N 1320 -430 1320 -400 { lab=vdd}
N 1320 -430 1330 -430 { lab=vdd}
N 1230 -170 1240 -170 { lab=vss}
N 1240 -170 1240 -140 { lab=vss}
N 1230 -140 1240 -140 { lab=vss}
N 1430 -170 1440 -170 { lab=vss}
N 1430 -170 1430 -140 { lab=vss}
N 1430 -140 1440 -140 { lab=vss}
N 1330 -330 1340 -330 { lab=NET_M12_M13}
N 1340 -330 1340 -300 { lab=NET_M12_M13}
N 1330 -300 1340 -300 { lab=NET_M12_M13}
N 450 270 470 270 { lab=clr}
N 450 270 450 490 { lab=clr}
N 420 490 450 490 { lab=clr}
N 1010 -350 1010 -320 { lab=n_clk}
N 1010 -90 1010 -70 { lab=clk}
N -50 -320 -50 -310 { lab=d}
N -80 -310 -50 -310 { lab=d}
N -80 -320 -80 -310 { lab=d}
N -50 -180 -50 -170 { lab=d}
N -90 -250 -90 -170 { lab=d}
N -90 -170 -80 -170 { lab=d}
N -90 -320 -90 -250 { lab=d}
N -90 -320 -80 -320 { lab=d}
N -20 -320 10 -320 { lab=NET_M0_M5}
N 10 -320 10 -250 { lab=NET_M0_M5}
N 10 -250 10 -170 { lab=NET_M0_M5}
N -20 -170 10 -170 { lab=NET_M0_M5}
N 260 190 260 200 { lab=NET_M0_M5}
N 230 200 260 200 { lab=NET_M0_M5}
N 230 190 230 200 { lab=NET_M0_M5}
N 260 330 260 340 { lab=NET_M0_M5}
N 220 260 220 340 { lab=NET_M0_M5}
N 220 340 230 340 { lab=NET_M0_M5}
N 220 190 220 260 { lab=NET_M0_M5}
N 220 190 230 190 { lab=NET_M0_M5}
N 290 190 320 190 { lab=NET_M4_M9}
N 320 190 320 260 { lab=NET_M4_M9}
N 320 260 320 340 { lab=NET_M4_M9}
N 290 340 320 340 { lab=NET_M4_M9}
N 1010 -280 1010 -270 { lab=NET_M2_M11}
N 980 -270 1010 -270 { lab=NET_M2_M11}
N 980 -280 980 -270 { lab=NET_M2_M11}
N 970 -210 970 -130 { lab=NET_M2_M11}
N 970 -130 980 -130 { lab=NET_M2_M11}
N 970 -280 970 -210 { lab=NET_M2_M11}
N 970 -280 980 -280 { lab=NET_M2_M11}
N 1040 -280 1070 -280 { lab=NET_M10_M19}
N 1070 -280 1070 -210 { lab=NET_M10_M19}
N 1070 -210 1070 -130 { lab=NET_M10_M19}
N 1040 -130 1070 -130 { lab=NET_M10_M19}
N 1480 190 1480 200 { lab=NET_M10_M19}
N 1450 200 1480 200 { lab=NET_M10_M19}
N 1450 190 1450 200 { lab=NET_M10_M19}
N 1480 330 1480 340 { lab=NET_M10_M19}
N 1440 260 1440 340 { lab=NET_M10_M19}
N 1440 340 1450 340 { lab=NET_M10_M19}
N 1440 190 1440 260 { lab=NET_M10_M19}
N 1440 190 1450 190 { lab=NET_M10_M19}
N 1510 190 1540 190 { lab=qb}
N 1540 190 1540 260 { lab=qb}
N 1540 260 1540 340 { lab=qb}
N 1510 340 1540 340 { lab=qb}
N 230 330 260 330 { lab=NET_M0_M5}
N 230 330 230 340 { lab=NET_M0_M5}
N 1450 330 1480 330 { lab=NET_M10_M19}
N 1450 330 1450 340 { lab=NET_M10_M19}
N 1010 -140 1010 -130 { lab=NET_M2_M11}
N 980 -140 1010 -140 { lab=NET_M2_M11}
N 980 -140 980 -130 { lab=NET_M2_M11}
N -80 -180 -50 -180 { lab=d}
N -80 -180 -80 -170 { lab=d}
N 1100 -250 1100 220 { lab=NET_M10_M19}
N 1750 -20 1790 -20 { lab=q}
N 1750 -250 1750 -20 { lab=q}
N 1850 -20 1940 -20 { lab=qb}
N 1940 -20 1940 220 { lab=qb}
C {ipin.sym} -50 -380 0 0 {name=p1 lab=clk}
C {lab_pin.sym} 260 400 3 0 {name=l4 sig_type=std_logic lab=clk
}
C {lab_pin.sym} 260 120 1 0 {name=l5 sig_type=std_logic lab=n_clk
}
C {lab_pin.sym} 1480 130 1 0 {name=l6 sig_type=std_logic lab=clk
}
C {lab_pin.sym} 1480 410 3 0 {name=l7 sig_type=std_logic lab=n_clk
}
C {sky130_fd_pr/nfet_01v8.sym} 230 -200 0 0 {name=M3
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 230 -310 0 0 {name=M2
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1830 30 0 0 {name=M17
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1830 -80 0 0 {name=M16
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {ipin.sym} -250 -250 0 0 {name=p8 lab=d
}
C {sky130_fd_pr/nfet_01v8.sym} 1210 -170 0 0 {name=M14
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1460 -170 0 1 {name=M15
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1310 -300 0 0 {name=M13
L=L1
W=W3
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1350 -400 0 1 {name=M12
L=L1
W=W3
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 510 300 0 0 {name=M8
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 760 300 0 1 {name=M9
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 610 170 0 0 {name=M7
L=L1
W=W3
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 650 70 0 1 {name=M6
L=L1
W=W3
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {opin.sym} 2070 -250 0 0 {name=p13 lab=q
}
C {opin.sym} 2060 220 0 0 {name=p14 lab=qb
}
C {lab_pin.sym} 250 -400 1 0 {name=l10 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 1330 -470 1 0 {name=l11 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 1850 -170 0 0 {name=l12 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 630 0 1 0 {name=l13 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 250 -120 3 0 {name=l14 sig_type=std_logic lab=vss}
C {lab_pin.sym} 630 390 3 0 {name=l15 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1330 -80 3 0 {name=l16 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1850 110 2 0 {name=l17 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1500 -310 2 0 {name=l19 sig_type=std_logic lab=clr
}
C {ipin.sym} 420 490 0 0 {name=p4 lab=clr}
C {lab_pin.sym} 1010 -350 1 0 {name=l2 sig_type=std_logic lab=n_clk
}
C {lab_pin.sym} 1010 -70 3 0 {name=l3 sig_type=std_logic lab=clk
}
C {sky130_fd_pr/pfet_01v8.sym} -50 -150 3 0 {name=M1
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -50 -340 1 0 {name=M0
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 260 360 3 0 {name=M5
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 260 170 1 0 {name=M4
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1010 -110 3 0 {name=M11
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1010 -300 1 0 {name=M10
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1480 360 3 0 {name=M19
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1480 170 1 0 {name=M18
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_wire.sym} 60 10 0 0 {name=l18 sig_type=std_logic lab=NET_M0_M5}
C {lab_wire.sym} 420 220 0 0 {name=l20 sig_type=std_logic lab=NET_M4_M9}
C {lab_wire.sym} 800 -110 0 0 {name=l21 sig_type=std_logic lab=NET_M2_M11}
C {lab_wire.sym} 1250 220 0 0 {name=l22 sig_type=std_logic lab=NET_M10_M19}
C {lab_wire.sym} 630 120 0 0 {name=l23 sig_type=std_logic lab=NET_M6_M7}
C {lab_wire.sym} 1330 -350 0 0 {name=l24 sig_type=std_logic lab=NET_M12_M13}
C {ipin.sym} -50 -100 0 0 {name=p5 lab=n_clk}
