v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -180 110 -180 160 { lab=vss}
N -180 160 100 160 { lab=vss}
N 100 60 100 160 { lab=vss}
N 100 -50 100 0 { lab=Z}
N 100 -200 100 -110 { lab=vdd}
N -240 -200 100 -200 { lab=vdd}
N -100 -200 -100 -170 { lab=vdd}
N -280 -200 -280 -170 { lab=vdd}
N -280 -200 -270 -200 { lab=vdd}
N -280 -110 -280 -80 { lab=#net1}
N -280 -80 -100 -80 { lab=#net1}
N -100 -110 -100 -80 { lab=#net1}
N -270 -200 -240 -200 { lab=vdd}
N -180 -80 -180 -40 { lab=#net1}
N -180 20 -180 50 { lab=#net2}
N 20 -80 60 -80 { lab=#net1}
N 20 -80 20 30 { lab=#net1}
N 20 30 60 30 { lab=#net1}
N -180 -60 20 -60 { lab=#net1}
N 100 30 110 30 { lab=vss}
N 110 30 110 60 { lab=vss}
N 100 60 110 60 { lab=vss}
N 100 -80 110 -80 { lab=vdd}
N 110 -110 110 -80 { lab=vdd}
N 100 -110 110 -110 { lab=vdd}
N -180 -10 -170 -10 { lab=#net2}
N -170 -10 -170 20 { lab=#net2}
N -180 20 -170 20 { lab=#net2}
N -190 80 -180 80 { lab=vss}
N -190 80 -190 110 { lab=vss}
N -190 110 -180 110 { lab=vss}
N -280 -140 -270 -140 { lab=vdd}
N -270 -170 -270 -140 { lab=vdd}
N -280 -170 -270 -170 { lab=vdd}
N -110 -140 -100 -140 { lab=vdd}
N -110 -170 -110 -140 { lab=vdd}
N -110 -170 -100 -170 { lab=vdd}
N -360 -140 -320 -140 { lab=in_a}
N -40 160 -40 200 { lab=vss}
N 100 -20 180 -20 { lab=Z}
N -60 -240 -60 -200 { lab=vdd}
N -60 -140 -10 -140 { lab=in_b}
N -140 80 -60 80 { lab=in_b}
N -60 -140 -60 80 { lab=in_b}
N -320 -10 -220 -10 { lab=in_a}
N -320 -140 -320 -10 { lab=in_a}
C {sky130_fd_pr/nfet_01v8.sym} -160 80 0 1 {name=M3
L=L1
W=W2
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -200 -10 0 0 {name=M2
L=L1
W=W2
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 80 30 0 0 {name=M5
L=L1
W=W1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -300 -140 0 0 {name=M0
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -80 -140 0 1 {name=M1
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 80 -80 0 0 {name=M4
L=L1
W=W2
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {ipin.sym} -360 -140 0 0 {name=p1 lab=in_a

}
C {ipin.sym} -40 200 3 0 {name=p4 lab=vss
}
C {ipin.sym} -60 -240 1 0 {name=p5 lab=vdd
}
C {opin.sym} 180 -20 0 0 {name=p6 lab=Z
}
C {ipin.sym} -20 -140 2 0 {name=p7 lab=in_b

}
C {param.sym} 230 -180 0 0 {name=W1 value="W1=0.45 W2=0.9 W3=1.8"}
